#!/usr/bin/env python
"""
This is a solution for the starting exercise of the workshop.

It reads in the 1D spectra and the redshift probability distribution and makes a plot
with 2 panels.

It then uses np.trapz to integrate the p(z) over a redshift interval and prints the values.

To run on the command line:
> python example1.py
From within python interpreter:
> import example1

"""


from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import astropy
from astropy.io import fits

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.integrate

### Read in data 
spec_data = fits.open('../Data/cosmos-01-G141_21477.1D.fits', format='fits')

wave = spec_data[1].data['wave']
flux = spec_data[1].data['flux']
contam = spec_data[1].data['contam']
sens = spec_data[1].data['sensitivity']

pz = fits.open('../Data/cosmos-01-G141_21477.new_zfit.pz.fits')

### Make the plot, this is going to have 2 panels: spec and p(z)

plt.figure(figsize=(10.,4.))
gs = gridspec.GridSpec(1,2)
ax1 = plt.subplot(gs[0,0])
ax1.plot(wave/1e4, (flux-contam)/sens)
ax1.set_xlim([1.1,1.65])
ax1.set_ylim([-0.1, 0.4])
ax1.set_xlabel('$\lambda$[$\mu$m]')
ax1.set_ylabel('$f_{\lambda}$')

ax2 = plt.subplot(gs[0,1])
ax2.plot(pz[1].data, np.exp(pz[2].data - np.max(pz[2].data)),'-', color='green')
#ax2.plot(pz[4].data, np.exp(pz[5].data - np.max(pz[5].data)),'-', color='blue')
ax2.set_xlabel(r'$z$')
ax2.set_ylabel(r'$p(z)$')
ax2.set_yscale('log')
ax2.set_yticklabels([])

plt.savefig('test_figure.png', dpi=300)

### Integrate probability 

mask = (pz[1].data > 2.1) & (pz[1].data < 2.3)

int_limit = np.trapz(np.exp(pz[2].data[mask]), x = pz[1].data[mask])
int_all = np.trapz(np.exp(pz[2].data), x = pz[1].data)
print ('p(2.1<z<2.3): {}'.format(int_limit))
print ('p(2.1<z<2.3)/p(total): {}'.format(int_limit/int_all))
