from fizzbuzz import fizzbuzz

def test_fizzbuzz_1():
	assert fizzbuzz(1) == 1
def test_fizzbuzz_2():
	assert fizzbuzz(2) == 2
def test_fizzbuzz_3():
	assert fizzbuzz(3) == 'fizz'
def test_fizzbuzz_4():
	assert fizzbuzz(4) == 4
def test_fizzbuzz_5():
	assert fizzbuzz(5) == 'buzz'
def test_fizzbuzz_10():
	assert fizzbuzz(10) == 'buzz'
def test_fizzbuzz_15():
	assert fizzbuzz(15) == 'fizzbuzz'
def test_fizzbuzz_900():
	assert fizzbuzz(900) == 'fizzbuzz'
def test_fizzbuzz_42():
	assert fizzbuzz(42) == 'fizz'